## CSI Benchmark

A helm chart launching and managing a scale test on Kubernetes with CSI.

Configuration is all set in values.yaml:
```yaml
storageClassName: geneva-cephfs-testing
podMemory: "3Gi"
benchmarkGroups:
  - shares: 5
    replicas: 5
    idle: true
```

The storage class referenced above must exist on the cluster - it
depends on the environment you're running in. For CERN kubernetes clusters, you
should have storage classes already deployed by default matching our CephFS
clusters.

For the other parameters:

* **podMemory**: set this to make sure a Pod fills up a node
* **benchmarkGroups.shares**: number of shares to be created
* **replicas**: number of Pods to be launched (replicas on a Deployment)
* **idle**: true if clients should do nothing, false to launch a kernel untar per
  share

### Launching

```bash
helm install --namespace kube-system -n bench ./csi-benchmark
```

### Gradual Scaling

Example for a gradual increase of the number of concurrent clients, setting the
helm values explicitly in a loop:
```bash
for n in `seq 5 5 100`; do
  echo "$(date) : scaling to ${n} replicas"
  helm upgrade --namespace kube-system --set benchmarkGroups[0].replicas=${n} --set benchmarkGroups[0].shares=20 bench ./csi-benchmark 2>&1>/dev/null
  while kubectl -n kube-system get pod | grep ContainerCreating 2>&1>/dev/null; do
    sleep 2
  done
done
```
